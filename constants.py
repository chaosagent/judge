import enum


class Enum(enum.Enum):
    def __str__(self):
        return self.name


class APIKeyType(Enum):
    jury = 'jury'
    observer = 'observer'


class JobStatus(Enum):
    queued = 'queued'
    cancelled = 'cancelled'
    started = 'started'
    awaiting_verdict = 'awaiting_verdict'
    finished = 'finished'


class JobVerdict(Enum):
    accepted = 'AC'
    ran = 'RAN'
    invalid_source = 'IS'
    wrong_answer = 'WA'
    time_limit_exceeded = 'TLE'
    memory_limit_exceeded = 'MLE'
    runtime_error = 'RTE'
    illegal_syscall = 'ISC'
    compilation_error = 'CE'
    judge_error = 'JE'
